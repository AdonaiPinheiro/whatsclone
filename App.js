import React, { Component } from "react";
import { createAppContainer, createStackNavigator } from "react-navigation";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import ReduxThunk from "redux-thunk";

//Reducers
import Reducers from "./src/Reducers";

//Screens
import Preload from "./src/routes/Preload";
import Home from "./src/routes/Home";
import Conversas from "./src/routes/Conversas";

let store = createStore(Reducers, applyMiddleware(ReduxThunk));

const StackNav = createStackNavigator(
  {
    Preload: {
      screen: Preload
    },
    Home: {
      screen: Home
    },
    Conversas: {
      screen: Conversas
    }
  },
  {
    initialRouteName: "Preload",
    defaultNavigationOptions: {
      title: "",
      header: null
    }
  }
);

const Container = createAppContainer(StackNav);

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Container />
      </Provider>
    );
  }
}
