import React, { Component } from "react";
import { Text, StyleSheet, View } from "react-native";

//Redux
import { connect } from "react-redux";

//Actions
import { checkLogin } from "../actions/AuthActions";

export class Conversas extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <View style={styles.container}>
        <Text>Conversas {this.props.status}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    margin: 10
  }
});

const mapStateToProps = state => {
  return {
    status: state.auth.status
  };
};

const ConversasConnect = connect(
  mapStateToProps,
  { checkLogin }
)(Conversas);

export default ConversasConnect;
