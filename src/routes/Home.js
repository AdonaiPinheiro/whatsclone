import React, { Component } from "react";
import { Text, StyleSheet, View } from "react-native";

//Redux
import { connect } from "react-redux";

//Actions
import { checkLogin } from "../actions/AuthActions";

export class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <View style={styles.container}>
        <Text>Home {this.props.status}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    margin: 10
  }
});

const mapStateToProps = state => {
  return {
    status: state.auth.status
  };
};

const HomeConnect = connect(
  mapStateToProps,
  { checkLogin }
)(Home);

export default HomeConnect;
