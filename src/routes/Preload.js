import React, { Component } from "react";
import { Text, StyleSheet, View } from "react-native";
import { NavigationActions, StackActions } from "react-navigation";

//Redux
import { connect } from "react-redux";

//Actions
import { checkLogin } from "../actions/AuthActions";

export class Preload extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.directPages = this.directPages.bind(this);
    this.props.checkLogin();
  }

  directPages() {
    switch (this.props.status) {
      case 1:
        this.props.navigation.dispatch(
          StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: "Conversas" })]
          })
        );
        break;
      case 2:
        this.props.navigation.dispatch(
          StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: "Home" })]
          })
        );
        break;
    }
  }

  componentDidMount() {
    this.directPages();
  }

  componentDidUpdate() {
    this.directPages();
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>Carregando {this.props.status}</Text>
      </View>
    );
  }
}

//Styles
const styles = StyleSheet.create({
  container: {
    margin: 10
  }
});

//Searching states from Reducers
const mapStateToProps = state => {
  return {
    status: state.auth.status
  };
};

const PreloadConnect = connect(
  mapStateToProps,
  { checkLogin }
)(Preload);

export default PreloadConnect;
