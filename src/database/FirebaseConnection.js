import firebase from 'firebase';

var config = {
    apiKey: "AIzaSyALqDjDYfqebC5jZqb293ykJf8P_2QnUYg",
    authDomain: "whatsclone-b8c41.firebaseapp.com",
    databaseURL: "https://whatsclone-b8c41.firebaseio.com",
    projectId: "whatsclone-b8c41",
    storageBucket: "whatsclone-b8c41.appspot.com",
    messagingSenderId: "203267830638"
  };
firebase.initializeApp(config);

export default firebase;